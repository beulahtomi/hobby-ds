-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 20, 2018 at 04:25 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `office`
--

-- --------------------------------------------------------

--
-- Table structure for table `office_tbl`
--

CREATE TABLE IF NOT EXISTS `office_tbl` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `hobbies` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `office_tbl`
--

INSERT INTO `office_tbl` (`id`, `first_name`, `last_name`, `email`, `username`, `password`, `hobbies`) VALUES
(1, 'tomi', 'mohd', 'mohammedtomiyin@gmail.com', 'root', '', ' heyyy'),
(2, '', '', '', '', '', ''),
(3, 'tomi', 'mohd', 'mohammedtomiyin@gmail.com', 'tomimm', '', ''),
(4, '', '', '', '', '', ''),
(5, 'tomiiii', 'mohd', 'mohammedtomiyin@gmail.com', 'tomim', 'hello', ''),
(6, '', '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', ''),
(8, '', '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', ''),
(9, 'hello', 'wawu', 'momhd.gmail.com', 'hello', 'hello', ''),
(10, 'hello', 'wawu', 'momhd.gmail.com', 'hello', 'hello', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
