-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 23, 2018 at 12:23 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `office`
--

-- --------------------------------------------------------

--
-- Table structure for table `hobby_tbl`
--

CREATE TABLE IF NOT EXISTS `hobby_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `hobby_tbl`
--

INSERT INTO `hobby_tbl` (`id`, `user_id`, `name`) VALUES
(3, 3, ' reading'),
(10, 3, ' test'),
(11, 3, ' game'),
(12, 3, ' game'),
(13, 3, ' game'),
(14, 3, ' tester'),
(15, 3, ' tester'),
(16, 3, ' again'),
(17, 7, 'cheche'),
(18, 7, ' hi'),
(19, 7, ' hello'),
(20, 8, ' Touch'),
(21, 9, ' going to school'),
(22, 9, ' dancing'),
(23, 9, ' cooking'),
(24, 9, ' ');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
