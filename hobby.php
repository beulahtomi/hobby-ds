<?php
    session_start();

    $id = $_SESSION['user_id'];
    $url = parse_url(getenv("CLEARDB_DATABASE_URL"));
    $server='us-cdbr-iron-east-01.cleardb.net';
    $username='b427b8e91f44aa';
    $password='7c190726';
    $db='heroku_9fe38e3a6f0d30b';

    //create connection
    $conn = new mysqli($server,$username,$password,$db);

    if($conn->connect_error){
        die("connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM `hobby_tbl` WHERE user_id = $id";

    // die($sql);

    $result = mysqli_query($conn, $sql);

    $saved_hobbies = array();

    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            array_push($saved_hobbies, $row);
        }
    }
?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">


    <title>Tomi | Hobby</title>

    <!-- Bootstrap core CSS -->
    
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="assets/css/cover.css" rel="stylesheet">

    <style type="text/css">
        .card {
            margin-bottom: 5px;
        }
    </style>
  </head>

  <body class="text-center">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <h3 class="masthead-brand">Hobby List</h3>
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link" href="index.php">Home</a>
            <a class="nav-link active" href="hobby.php">Hobby List</a>
            <a class="nav-link" href="logout.php">Log Out</a>
          </nav>
        </div>
      </header>

      <main role="main" class="inner cover">
       
        <h1 class="cover-heading">Hobby List</h1>
            <div class="row">
            <?php
                foreach($saved_hobbies as $hobby)
                {
                    
                    echo "<div class='col-md-6'><div class='card' style='color:black'>" . $hobby['name']. "</div></div>";
                    echo "<br>";
                }   
            ?> 
        </div>
        <p class="lead">
          <a href="home.php" class="btn btn-lg btn-secondary">Add Hobby</a>
        </p>
      </main>

      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>Hobby, by <a href="https://twitter.com/mdo">@tomi</a>.</p>
        </div>
      </footer>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
  </body>
</html>


