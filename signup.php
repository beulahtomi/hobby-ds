<?php
  session_start();
  
  if (isset($_SESSION['user_id']) || isset($_SESSION['username']) || isset($_SESSION['email'])) {
    header('Location: hobby.php');
  }
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">


    <title>Tomi | Hobby</title>

    <!-- Bootstrap core CSS -->
    
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="assets/css/cover.css" rel="stylesheet">
  </head>

  <body class="text-center">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <h3 class="masthead-brand">Hobby</h3>
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link" href="index.php">Home</a>
            <a class="nav-link active" href="signup.php">Sign Up</a>
            <a class="nav-link" href="login.php">Log in</a>
          </nav>
        </div>
      </header>

      <main role="main" class="inner cover">
       
        <h1 class="cover-heading">Sign Up</h1>
        <br>
<div>
<form  action="" method="post">
	<div class="form-group row">
		<label class="control-label" style="font-weight: bold;">First Name</label>
        <input type="text" class="form-control" name="first_name" placeholder="First name" required >
    </div>

	<div class="form-group row">
		<label class="control-label" style="font-weight: bold;">Last Name</label>
        <input type="text" class="form-control" name="last_name" placeholder="Last name" required >
    </div>
	
	<div class="form-group row">
		<label class="control-label" style="font-weight: bold;">Email</label>
        <input type="email" class="form-control" name="email" placeholder="email" required >
    </div>

	<div class="form-group row">
		<label class="control-label" style="font-weight: bold;"> Username</label>
        <input type="text" class="form-control" name="username" placeholder="Username" required >
    </div>

	<div class="form-group row">
		<label class="control-label" style="font-weight: bold;"> Password</label>
        <input type="password" class="form-control" name="password" placeholder="Password" required >
    </div>
	
	
	<input type="submit" name="submit" class="btn btn-md btn-default" value="Submit">
	</form>
</div>
        
      </main>
<br>
      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>Hobby, by <a href="https://twitter.com/mdo">@tomi</a>.</p>
        </div>
      </footer>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
  </body>
</html>
<?php 
if(isset($_POST['submit'])){
	$first_name=$_POST['first_name'];
	$last_name=$_POST['last_name'];
	$email=$_POST['email'];
	$username=$_POST['username'];
	$password=$_POST['password'];
	//$password1=$_POST['password1'];
	
	
	  $url = parse_url(getenv("CLEARDB_DATABASE_URL"));
    $server='us-cdbr-iron-east-01.cleardb.net';
    $username='b427b8e91f44aa';
    $password='7c190726';
    $db='heroku_9fe38e3a6f0d30b';

    //create connection
    $conn = new mysqli($server,$username,$password,$db);


	if($conn->connect_error){
		die("connection failed: " . $conn->connect_error);	
	}

	$sql="INSERT into user_table(first_name,last_name,email,username,password) values('$first_name','$last_name','$email','$username','$password')";

	if($conn->query($sql)===TRUE){
		$last_id = $conn->insert_id;
		header( 'Location: login.php');
	}
	else{
	 	echo "didnt work";
 	}
}
?>


<!-- 

<html>
<head>
<title>Tomi | Hobby</title>
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

</head>
<body style="padding-top: 70px; padding-bottom: 70px;">
	<nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
    <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse"> 
    <span class="sr-only">Toggle navigation</span> 
    <span class="icon-bar"></span> 
    <span class="icon-bar"></span> 
    <span class="icon-bar"></span> </button>
    <a class="navbar-brand" href="index.php" style="color:blue">Sign Up/Login in</a> 
    </div> 
</div>
</nav>
	<div>
	<form  action="" method="post"  class="container jumbotron">
	 <center> <h1> HOBBY </h1> </center>
    <center><h6 style="color:blue">Not a member yet?</h6>
    	<a href="signup.php" class="btn btn-md btn-default" role="button">Sign Up</a></center>

    <center><h6 style="color:blue">Are you a member?</h6>
    	<a href="Login.php" class="btn btn-md btn-default" role="button">Login</a>
    	</center>
	</form>
</div>
<nav class="navbar navbar-default navbar-inverse navbar-fixed-bottom">
  <div class="container">
    <div class="navbar-header"><a class="navbar-brand" style="color:blue">Hobby</a></div>
    <ul class="nav navbar-nav navbar-right"> 
    <li> &copy; 2018. All rights reserved.</a></li>
  </ul> 
  </div>
</nav>
</div>

<script src="js/bootstrap.min.js"></script>
</body>
</html>
</body>
</html> -->
<!-- <html>
<head>
<title>Tomi | Hobby</title>
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

</head>
<body>
	<nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
    <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse"> 
    <span class="sr-only">Toggle navigation</span> 
    <span class="icon-bar"></span> 
    <span class="icon-bar"></span> 
    <span class="icon-bar"></span> </button>
    <a class="navbar-brand" href="signup.php" style="color:blue">Sign Up</a> 
    </div> 
 <div class="collapse navbar-collapse" id="example-navbar-collapse"> 
      <ul class="nav navbar-nav navbar-right"> 
        <li><a href="index.php"> Home </a></li>  
        <li><a href="login.php"> Login </a></li>    
     </ul> 
    </div>
</div>
</nav>

<br>
<div class="container jumbotron">
<form  action="" method="post">
	<div class="form-group has-feedback">
		<label class="control-label" style="font-weight: bold;">First Name</label>
        <input type="text" class="form-control" name="first_name" required >
    </div>

	<div class="form-group has-feedback">
		<label class="control-label" style="font-weight: bold;">Last Name</label>
        <input type="text" class="form-control" name="last_name" required >
    </div>
	
	<div class="form-group has-feedback">
		<label class="control-label" style="font-weight: bold;">Email</label>
        <input type="email" class="form-control" name="email" required >
    </div>

	<div class="form-group has-feedback">
		<label class="control-label" style="font-weight: bold;"> Username</label>
        <input type="text" class="form-control" name="username" required >
    </div>

	<div class="form-group has-feedback">
		<label class="control-label" style="font-weight: bold;"> Password</label>
        <input type="password" class="form-control" name="password" required >
    </div>
	
	
	<input type="submit" name="submit" class="btn btn-md btn-default" value="Submit">
	</form>
</div>
<nav class="navbar navbar-default navbar-inverse navbar-fixed-bottom">
  <div class="container">
    <div class="navbar-header"><a class="navbar-brand" style="color:blue">Hobby</a></div>
    <ul class="nav navbar-nav navbar-right"> 
    <li> &copy; 2018. All rights reserved.</a></li>
  </ul> 
  </div>
</nav>
</body>
</html>

<?php 
if(isset($_POST['submit'])){
	$first_name=$_POST['first_name'];
	$last_name=$_POST['last_name'];
	$email=$_POST['email'];
	$username=$_POST['username'];
	$password=$_POST['password'];
	//$password1=$_POST['password1'];
	
	
	$servename="localhost";
	$usernamedb="root";
	$passworddb="";
	$dbname="office";

	//create connection
	$conn = new mysqli($servename,$usernamedb,$passworddb,$dbname);

	if($conn->connect_error){
		die("connection failed: " . $conn->connect_error);	
	}

	$sql="INSERT into user_table(first_name,last_name,email,username,password) values('$first_name','$last_name','$email','$username','$password')";

	if($conn->query($sql)===TRUE){
		$last_id = $conn->insert_id;
		header( 'Location: login.php');
	}
	else{
	 	echo "didnt work";
 	}
}
?> -->