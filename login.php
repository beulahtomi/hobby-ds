<?php
  session_start();
  
  if (isset($_SESSION['user_id']) || isset($_SESSION['username']) || isset($_SESSION['email'])) {
    header('Location: signup.php');
  }
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">


    <title>Tomi | Hobby</title>

    <!-- Bootstrap core CSS -->
    
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="assets/css/cover.css" rel="stylesheet">
  </head>

  <body class="text-center">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <h3 class="masthead-brand">Hobby</h3>
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link" href="index.php">Home</a>
            <a class="nav-link" href="signup.php">Sign Up</a>
            <a class="nav-link active" href="login.php">Log in</a>
          </nav>
        </div>
      </header>

      <main role="main" class="inner cover">
       
        <h1 class="cover-heading">Log In</h1>
        <div>
        <form  action="" method="post">
          <div class="form-group row">
            <label class="control-label" style="font-weight: bold;"> Username</label>
                <input type="text" class="form-control" name="username" placeholder="Username" required >
            </div>

          <div class="form-group row">
            <label class="control-label" style="font-weight: bold;"> Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password" required >
            </div>
          
          <input type="submit" name="submit" class="btn btn-md btn-default" value="Submit">
        </form>

  <?php
  $url = parse_url(getenv("CLEARDB_DATABASE_URL"));
    $server='us-cdbr-iron-east-01.cleardb.net';
    $username='b427b8e91f44aa';
    $password='7c190726';
    $db='heroku_9fe38e3a6f0d30b';

    //create connection
   
 if(isset($_POST['submit'])){
              $username = $_POST['username'];
              $password = $_POST['password'];
              if(!empty($username) && !empty($password)){
                  $conn = new mysqli($server,$username,$password,$db) or die("cant connect");
                $sql="select * from user_table";
                $query =mysqli_query($con,$sql);

                while($row = mysqli_fetch_array($query)){
                  $u=$row['username'];
                  $p=$row['password'];
                  $e=$row['email'];
                  $_SESSION['user_id'] = $row['id'];
                }
                if ($username==$u && $password == $p) {
                  # code...
                
        echo ("<script language='javascript'>
                       window.location.href='hobby.php';                       
                       </script>");       
                       $_SESSION['username']=$u;
                      // $_SESSION['id']=$f;
                       $_SESSION['email']=$l;
                }
              else{
                echo "<p class='alert alert-danger' style='width: 400px;' >This User does not exist. Please Check and try again.</p>";      

              }
            }else{
              echo "<p class='alert alert-danger' style='width: 400px;'>complete the form</p>";
            }
        }
          
?>


        
      </main>

      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>Hobby, by <a href="https://twitter.com/mdo">@tomi</a>.</p>
        </div>
      </footer>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
  </body>
</html>